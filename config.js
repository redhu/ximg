exports.tmproot='/tmp/uptmp'; //上传临时目录
exports.imgroot='/home/www/upload'; //图片存储目录
exports.errorlog='/home/www/error.log'; //程序错误日志，记录

exports.port= 8050;
exports.appname= 'ximg';
exports.maxFileSize= 1024*1024*10;//10mb
exports.maxSide= 800; //最大截图边长
exports.minSide= 30; //最小截图边长

exports.rootpath = "http://192.168.57.186/";

exports.imgtypes={
        "gif": "image/gif",
        "jpeg": "image/jpeg",
        "jpg": "image/jpeg",
        "png": "image/png"
};