/**
 * Module dependencies.
 */

var express = require('express');
var upload  = require('./routes/upload');
var http    = require('http');
var path    = require('path');
var config  = require("./config");

var fs       = require('fs');
var errorLog = fs.createWriteStream(config.errorlog, {flags: 'a'});

process.on('uncaughtException', function (err) {
    console.trace(err);
    errorLog.write('\n[' + new Date + ']' + 'Caught exception: ' + err);
});

var app = express();


app.use(express.logger('dev'));

app.use(express.json());
app.use(express.urlencoded());
app.use(express.multipart({
    uploadDir: config.tmproot
}));


app.use(express.methodOverride());
app.use(app.router);



// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

//index,
app.get('/', function(req,res,next){
	res.end("Amazing!");
});
app.get(/^\/\d{1,10}\/upload$/, function(req,res,next){
	res.end("Doing!");
});

//get img
//app.get(/^\/\d{1,6}\/[0-9a-f]{32}(?:-\d+-\d+)?(-f)?\.(jpg|jpeg|gif|png)$/, img.read);


//img manage
//app.get(/^\/\d{1,6}\/[0-9a-f]{32}(?:-\d+-\d+)?(-f)?\.(jpg|jpeg|gif|png)\/manage-(tleft|tright|del|resize|info)$/, manage.exec);


//img upload
app.post(/^\/\d{1,10}\/upload$/, upload.exec);


http.createServer(app).listen(config.port, function () {
    console.log('%s:%s',new Date(),'server listening:' + config.port);
});



